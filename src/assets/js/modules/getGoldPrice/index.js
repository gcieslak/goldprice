const axios = require("axios");
const goldRateModule = () => {
  let inputDate = document.getElementById("dateInput");
  let form = document.getElementById("goldRateForm");
  const blockCallendar = (input, scope) => {
    let today = new Date().toISOString().split("T")[0];
    input.setAttribute(scope, today);
  };
  blockCallendar(inputDate, "max");

  const addResulTtoDom = (data) => {
    let element = document.getElementById("golderRateResult");
    if (element.hasChildNodes()) {
      let firstChild = element.firstChild;
      firstChild.remove();
    }
    let tag = document.createElement("p");
    let text = document.createTextNode(
      `Gold rate as of ${data.data}: ${data.cena} zł`
    );
    tag.appendChild(text);
    element.appendChild(tag);
  };

  form.addEventListener("submit", function(event) {
    event.preventDefault();
    if (inputDate.value !== "") {
      axios
        .get(`http://api.nbp.pl/api/cenyzlota/${inputDate.value}?format=json`)
        .then(function(response) {
          console.log(response.data[0]);
          addResulTtoDom(response.data[0]);
        })
        .catch(function(error) {
          console.log(error);
        })
        .finally(function() {});
      inputDate.value = "";
    }
  });
};

export default goldRateModule;
